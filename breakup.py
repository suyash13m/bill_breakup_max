import pandas as pd
import numpy as np
import os
 
dirpath="/Users/suyashgarg/Documents/git_repo/bill_breakup_max"
#dirpath = os.getcwd()
data_path=dirpath+"/Max packages.xlsx"

xl = pd.ExcelFile(data_path)

def room_cst(mylist):
    gen_info = xl.parse('General Information')
    gen_info = gen_info.iloc[2:] 
    list(gen_info)
    gen_info.reset_index(drop = True, inplace = True)
    vect=[]
    for i in range(gen_info.shape[0]):
        vect.append(mylist[2] in gen_info['Unnamed: 1'][i])
    room_cst=gen_info['Unnamed: 2'][vect]
    room_cst=pd.DataFrame(room_cst)
    room_cst.reset_index(drop = True, inplace = True)
    room_cst=room_cst['Unnamed: 2'][0]
    return room_cst

def icu_cst(mylist):
    gen_info = xl.parse('General Information')
    gen_info = gen_info.iloc[2:] 
    list(gen_info)
    gen_info.reset_index(drop = True, inplace = True)

    vect=[]
    for i in range(gen_info.shape[0]):
        vect.append('ICU' in gen_info['Unnamed: 1'][i])
    icu_cst=gen_info['Unnamed: 2'][vect]
    icu_cst=pd.DataFrame(icu_cst)
    icu_cst.reset_index(drop = True, inplace = True)
    icu_cst=icu_cst['Unnamed: 2'][0]
    return icu_cst

def consult_cst(mylist):
    gen_info = xl.parse('General Information')
    gen_info = gen_info.iloc[2:] 
    list(gen_info)
    gen_info.reset_index(drop = True, inplace = True)
    vect=[]
    for i in range(gen_info.shape[0]):
        vect.append(mylist[2] in gen_info['Unnamed: 1'][i])
    consult_cst=gen_info['Unnamed: 3'][vect]
    consult_cst=pd.DataFrame(consult_cst)
    consult_cst.reset_index(drop = True, inplace = True)
    consult_cst=consult_cst['Unnamed: 3'][0]
    consult_cst=consult_cst*(mylist[4])+1400*mylist[3]
    return consult_cst


def invest_lmt(mylist):
    package = xl.parse('Packages')
    #package = package.iloc[3:] 
    package=package.dropna(axis=0, how='any')
    package.reset_index(drop = True, inplace = True)
    package['Unnamed: 1']=package['Unnamed: 1'].str.lower()
    
    vect=[]
    for i in range(package.shape[0]):
        vect.append(mylist[1] in package['Unnamed: 1'][i])

    list(package)
    flag=package['Unnamed: 9'][vect]
    flag.reset_index(drop = True, inplace = True)
    invest_lmt=flag[0]
    return invest_lmt


def pharm_lmt(mylist):
    package = xl.parse('Packages')
    #package = package.iloc[3:] 
    package=package.dropna(axis=0, how='any')
    package.reset_index(drop = True, inplace = True)
    package['Unnamed: 1']=package['Unnamed: 1'].str.lower()
    
    vect=[]
    for i in range(package.shape[0]):
        vect.append(mylist[1] in package['Unnamed: 1'][i])

    flag=package['Unnamed: 8'][vect]
    flag.reset_index(drop = True, inplace = True)
    pharm_lmt=flag[0]
    return pharm_lmt

def surgeons_fees(mylist):
    surg_procedure  = xl.parse('Surgical Procedures')
    list(surg_procedure)
    del surg_procedure['Unnamed: 9']
    del surg_procedure['Unnamed: 10']
    del surg_procedure['Unnamed: 11']
    del surg_procedure['Unnamed: 12']
    del surg_procedure['Unnamed: 13']

    surg_procedure=surg_procedure.dropna(axis=0, how='any')
    
    
    #surg_procedure['Unnamed: 2']=surg_procedure['Unnamed: 2'].str.lower()
        
    surg_procedure=surg_procedure[(surg_procedure['Unnamed: 2']== "Angioplasty Stenting") | (surg_procedure['Unnamed: 2']== "Lap Cholecystectomy") 
    | (surg_procedure['Unnamed: 2']== "Emergency Surgery/CABG/Post MI VSD") | (surg_procedure['Unnamed: 2']== "Inguinal Hernia Repair Reinforcement with Marlex or Prolene Mesh (Unilateral)")]
    surg_procedure['Unnamed: 2']=surg_procedure['Unnamed: 2'].str.lower()
    surg_procedure.reset_index(drop = True, inplace = True)

    vect=[]
    for i in range(surg_procedure.shape[0]):
        vect.append(mylist[1] in surg_procedure['Unnamed: 2'][i])
        
    if mylist[2]=="Economy":
        flag=surg_procedure['Unnamed: 4'][vect]
        flag.reset_index(drop = True, inplace = True)
        surgeons_fees=flag[0]
    
    if mylist[2]=="Double":
        flag=surg_procedure['Unnamed: 5'][vect]
        flag.reset_index(drop = True, inplace = True)
        surgeons_fees=flag[0]
    
    if mylist[2]=="Single":
        flag=surg_procedure['Unnamed: 6'][vect]
        flag.reset_index(drop = True, inplace = True)
        surgeons_fees=flag[0]
    
    if mylist[2]=="Classic Deluxe":
        flag=surg_procedure['Unnamed: 7'][vect]
        flag.reset_index(drop = True, inplace = True)
        surgeons_fees=flag[0]
    
    if mylist[2]=="Suite":
        flag=surg_procedure['Unnamed: 8'][vect]
        flag.reset_index(drop = True, inplace = True)
        surgeons_fees=flag[0]
        
    return surgeons_fees

def icu_net_cst(mylist):
    icu_plus_ward=mylist[0]-pharm_lmt(mylist)-invest_lmt(mylist)-consult_cst(mylist)-surgeons_fees(mylist)
    icu_divide_ward=(icu_cst(mylist)*mylist[3])/(room_cst(mylist)*mylist[4])
    icu_net_cst=(icu_divide_ward*icu_plus_ward)/(1+icu_divide_ward)
    if icu_net_cst<0:
        icu_net_cst=0
    return icu_net_cst

def ward_net_cst(mylist):
    icu_plus_ward=mylist[0]-pharm_lmt(mylist)-invest_lmt(mylist)-consult_cst(mylist)-surgeons_fees(mylist)
    ward_net_cst=icu_plus_ward-icu_net_cst(mylist)
    if ward_net_cst<0:
        ward_net_cst=0
    return ward_net_cst


#bill, proceudre, room type, icu, ward
#mylist=input()
mylist=[350000,'chole','Classic Deluxe',1,2]

print("ward cost= {0}\nicu cost = {1}\npharm_lmt={2}\ninvest_lmt={3}\nconsult_net_cst={4}\nsurgeons_fees={5} " .format(ward_net_cst(mylist),icu_net_cst(mylist),pharm_lmt(mylist),invest_lmt(mylist),consult_cst(mylist),surgeons_fees(mylist)))